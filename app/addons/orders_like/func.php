<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_orders_like_get_product_data(&$product_id, $field_list, &$join, $auth, $lang_code, $condition) {

    $join .= " LEFT JOIN ?:products as products ON products.product_id = ?:products.product_id";

    $product_data = db_get_row("SELECT * FROM ?:products WHERE product_id = ?i", $product_id);

    return $product_data;

}

function fn_orders_like_get_product_data_post(&$product_data, $auth, $preview, $lang_code) {

}

function fn_orders_like_get_liked(&$product_data, &$auth) {
     
     $rating = $product_data['rating'] + 1;
     
     $product_id = $product_data['product_id'];
     
     $user_id = $auth;

     db_query("UPDATE ?:products SET rating = ?i WHERE product_id = ?i", $rating, $product_id);
     
     db_query("INSERT IGNORE INTO ?:products_like SET user_id = ?i, product_id = ?i, status = 1", $user_id, $product_id);
     
     fn_set_notification('W', __('notice'), ("LIKE"));

     return $rating;
}


function fn_orders_like_is_liked(&$auth, &$product_id) {

    $user_id = $auth['user_id'];
    

    $is_liked = db_get_row("SELECT status FROM ?:products_like WHERE user_id = ?i AND product_id = ?i", $user_id, $product_id);

    if($is_liked['status']) {
        
        return true;
    
    }else{ 
        
        return false; }

}

    