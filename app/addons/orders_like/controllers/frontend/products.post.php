<?php

/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry; 

$product_id = $_REQUEST['product_id'];

if ($mode == 'like') { 
 
    if (!$auth['user_id']) {
        
        fn_set_notification('E', __('error'), ('Вы не авторизованы!'));
        
        return array(CONTROLLER_STATUS_REDIRECT, "products.view");

        }else{   
            
            $is_liked = fn_orders_like_is_liked($auth, $product_id);

            if($is_liked) {
                
                fn_set_notification('W', __('notice'), ('Вы уже оценили этот товар!'));
                
                return array(CONTROLLER_STATUS_REDIRECT, "products.view");
                
                }else{                    
                        
                        $product_data = fn_orders_like_get_product_data($product_id);
                         
                        $rating = fn_orders_like_get_liked($product_data, $auth['user_id']);
         
                        Tygh::$app['view']->assign('rating', $rating);

                return array(CONTROLLER_STATUS_OK, "products.view");
        }
    }    
}   
    
